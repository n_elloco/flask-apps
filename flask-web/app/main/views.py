from flask import (
    abort, render_template, redirect, url_for, flash, request, current_app)
from flask_login import login_required, current_user

from app import db
from app.helpers import admin_required
from app.models import User, Role, Permission, Post

from . import main
from .forms import PostForm, EditProfileForm, EditProfileAdminForm


@main.route('/', methods=['GET', 'POST'])
def index():
    form = PostForm()
    if current_user.can(Permission.WRITE_ARTICLES) and form.validate_on_submit():
        post = Post(
            body=form.body.data,
            author=current_user._get_current_object()
        )
        db.session.add(post)
        return redirect(url_for('.index'))
    page = request.args.get('page', 1, type=int)
    pagination = Post.query.order_by(
        Post.timestamp.desc()
    ).paginate(
        page, per_page=current_app.config['FLASKY_POST_PER_PAGE'], error_out=False
    )
    posts = pagination.items
    return render_template(
        'index.html',
        form=form,
        posts=posts,
        pagination=pagination
    )


@main.route('/user/<username>')
def user(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        abort(404)
    posts = user.posts.order_by(Post.timestamp.desc()).all()
    return render_template('user.html', user=user, posts=posts)


@main.route('/edit-profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm()
    if form.validate_on_submit():
        for field_name in form.data:
            if field_name != 'submit':
                setattr(current_user, field_name, form.data[field_name])
        db.session.add(current_user)
        flash('Your profile has been updated')
        return redirect(url_for('.user', username=current_user.username))

    for field_name in form.data:
        if field_name != 'submit':
            getattr(form, field_name).data = getattr(current_user, field_name)
    return render_template('edit_profile.html', form=form)


@main.route('/edit-profile/<int:user_id>', methods=['GET', 'POST'])
@login_required
@admin_required
def edit_profile_admin(user_id):
    _user = User.query.get_or_404(user_id)
    form = EditProfileAdminForm(_user)

    if form.validate_on_submit():
        for field_name in form.data:
            if field_name not in ('submit', 'role'):
                setattr(_user, field_name, form.data[field_name])
        _user.role = Role.query.get(form.role.data)
        db.session.add(_user)
        flash('The profile has been updated')
        return redirect(url_for('.user', username=_user.username))

    for field_name in form.data:
        if field_name not in ('submit', 'role'):
            getattr(form, field_name).data = getattr(_user, field_name)
    form.role.data = _user.role_id
    return render_template('edit_profile.html', form=form, user=_user)


@main.route('/post/<int:post_id>')
def post(post_id):
    _post = Post.query.get_or_404(post_id)
    return render_template('post.html', posts=[_post])


@main.route('/edit/<int:post_id>', methods=['GET', 'POST'])
@login_required
def post_edit(post_id):
    _post = Post.query.get_or_404(post_id)

    if current_user != _post.author or not current_user.can(Permission.ADMIN):
        abort(403)

    form = PostForm()
    if form.validate_on_submit():
        _post.body = form.body.data
        db.session.add(_post)
        flash('The post has been updated')
        return redirect(url_for('.post', post_id=post_id))

    form.body.data = _post.body
    return render_template('edit_post.html', form=form)
