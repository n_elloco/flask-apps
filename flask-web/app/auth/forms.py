from flask_wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import (
    Email, DataRequired, Length, Regexp, EqualTo, ValidationError)

from app.models import User


class LoginForm(Form):
    email = StringField(
        'Email', validators=[DataRequired(), Email(), Length(1, 64)])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Keep me logged in')
    submit = SubmitField('Log In')


class RegistrationForm(Form):
    email = StringField(
        'Email', validators=[DataRequired(), Email(), Length(1, 64)])
    username = StringField(
        'Username',
        validators=[
            DataRequired(),
            Length(1, 64),
            Regexp(
                '^[A-z][A-z0-9_.]*$',
                message='Username mast have only letters, dots and underscores')
        ])
    password = PasswordField(
        'Password', validators=[
            DataRequired(), EqualTo('password2', 'Passwords must match')])
    password2 = PasswordField('Confirm password', validators=[DataRequired()])
    submit = SubmitField('Register')

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError('Email already registered')

    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError('Username already in use')


class ChangePasswordForm(Form):
    old_password = PasswordField('Old password', validators=[DataRequired()])
    password = PasswordField(
        'New password', validators=[
            DataRequired(), EqualTo('password2', 'Passwords must match')])
    password2 = PasswordField('Confirm password', validators=[DataRequired()])
    submit = SubmitField('Change password')


class ResetPasswordRequestForm(Form):
    email = StringField(
        'Email', validators=[DataRequired(), Email(), Length(1, 64)])
    submit = SubmitField('Reset password')


class ResetPasswordForm(Form):
    email = StringField(
        'Email', validators=[DataRequired(), Email(), Length(1, 64)])
    password = PasswordField(
        'New password', validators=[
            DataRequired(), EqualTo('password2', 'Passwords must match')])
    password2 = PasswordField('Confirm password', validators=[DataRequired()])
    submit = SubmitField('Change password')
